<?php


namespace App\Controller;

use App\Entity\Page;
use App\Entity\Image;
use App\Entity\Content;
use App\Entity\Contact;
use App\Type\ContactType;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManagerInterface as EntityManager;

class SiteController extends AbstractController
{

    /**
     * @Route("/", name="index")
     * @param EntityManager $entityManager
     * @param Request $request
     */
    public function index(EntityManager $entityManager, Request $request, \Swift_Mailer $mailer): Response
    {
        $form = $this->createSomeForm(ContactType::class);
        $form->handleRequest($request);
        if ($request->isMethod('POST')) {
            $this->sendEmail($form, $mailer );
        }
        $pages = $this->giveMeRepository($entityManager, Page::class, "allBy", "enabled", true);
        $banner = $this->giveMeRepository($entityManager, Image::class, "one", "title", "banner");
        $contents = $this->giveMeRepository($entityManager, Content::class, "all");
        $response = $this->render('page/page.html.twig', [
            'form' => $form->createView(),
            'pages' => $pages,
            'banner' => $banner,
            'contents' => $contents
        ]);

         // cache for 3600 seconds
        $response->setSharedMaxAge(31536000);
        return $response;
    }


    private function giveMeRepository($entityManager, $entity, string $options, $field = null, $fieldName = null)
    {
        $repository = $entityManager->getRepository($entity);
        if ($options == "all") {
            return $repository->findAll();
        }

        if ($options == "one") {
            return $repository->findOneBy(array($field => $fieldName));
        }

        if ($options == "allBy") {
            return $repository->findBy(array($field => $fieldName));
        }
    }

    private function createSomeForm($entity) {
        $contact = new Contact();
         $form = $this->createForm($entity, $contact);
         return $form;
    }

    private function sendEmail($form,  $mailer) {

            $msg = null;
            if ($form->isSubmitted() && $form->isValid()) {
                $message = (new \Swift_Message($form->get('question')->getData()))
                    ->setFrom($form->get('email')->getData())
                    ->setTo('albertjurasik@gmail.com')
                    ->setBody($this->renderView('mailer/send.html.twig', [
                        'email' => $form->get('email')->getData(),
                        'question' => $form->get('question')->getData(),
                        'theme' => $form->get('theme')->getData(),
                    ]), 'text/html');

                $mailer->send($message); // sned
                $msg = 'Wiadomość została wysłana.';
                $this->addFlash('success', $msg);
            } else {
                $this->get('session')->getFlashBag()->clear();
                $msg = 'Nie udało się wysłać wiadomości. Spróbuj ponownie za chwilę.';
                return $this->addFlash(
                    'danger',
                    $msg
                );
            }

    }

}