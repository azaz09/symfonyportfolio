<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200530174508 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE page (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(64) NOT NULL, slug VARCHAR(128) NOT NULL, titleEng VARCHAR(64) NOT NULL, language TINYINT(1) DEFAULT NULL, enabled TINYINT(1) DEFAULT NULL, created_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_140AB6202B36786B (title), UNIQUE INDEX UNIQ_140AB620989D9B62 (slug), UNIQUE INDEX UNIQ_140AB620CD666666 (titleEng), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE image (id INT AUTO_INCREMENT NOT NULL, content_id INT DEFAULT NULL, title VARCHAR(255) DEFAULT NULL, image_name VARCHAR(255) DEFAULT NULL, source VARCHAR(255) DEFAULT NULL, image_size INT DEFAULT NULL, type TINYINT(1) DEFAULT NULL, link VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_C53D045F2B36786B (title), UNIQUE INDEX UNIQ_C53D045FAC199498 (image_name), INDEX IDX_C53D045F84A0A3ED (content_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE description (id INT AUTO_INCREMENT NOT NULL, content_id INT DEFAULT NULL, `description` VARCHAR(768) NOT NULL, `description_eng` VARCHAR(768) DEFAULT NULL, `created_at` DATETIME NOT NULL, UNIQUE INDEX UNIQ_6DE440263782B5DB (`description`), INDEX IDX_6DE4402684A0A3ED (content_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(180) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', password VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D649F85E0677 (username), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE content (id INT AUTO_INCREMENT NOT NULL, page_id INT DEFAULT NULL, header VARCHAR(64) NOT NULL, header_eng VARCHAR(64) NOT NULL, created_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_FEC530A96E72A8C1 (header), UNIQUE INDEX UNIQ_FEC530A97A07A961 (header_eng), INDEX IDX_FEC530A9C4663E4 (page_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE image ADD CONSTRAINT FK_C53D045F84A0A3ED FOREIGN KEY (content_id) REFERENCES content (id)');
        $this->addSql('ALTER TABLE description ADD CONSTRAINT FK_6DE4402684A0A3ED FOREIGN KEY (content_id) REFERENCES content (id)');
        $this->addSql('ALTER TABLE content ADD CONSTRAINT FK_FEC530A9C4663E4 FOREIGN KEY (page_id) REFERENCES page (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE content DROP FOREIGN KEY FK_FEC530A9C4663E4');
        $this->addSql('ALTER TABLE image DROP FOREIGN KEY FK_C53D045F84A0A3ED');
        $this->addSql('ALTER TABLE description DROP FOREIGN KEY FK_6DE4402684A0A3ED');
        $this->addSql('DROP TABLE page');
        $this->addSql('DROP TABLE image');
        $this->addSql('DROP TABLE description');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE content');
    }
}
