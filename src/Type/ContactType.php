<?php


namespace App\Type;

use App\Entity\Contact;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\Email;

class ContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, ['label' => "Email", 'constraints' => [
                new Email(),
                new NotBlank(),
                new Length(['min' => 6, 'max' => 150]),
            ],
            ])
            ->add('question', TextType::class, ['label' => 'Pytania', 'constraints' => [
                new NotBlank(),
                new Length(['min' => 5, 'max' => 40]),
            ],
            ])
            ->add('theme', TextareaType::class, ['label' => 'Temat', 'constraints' => [
                new NotBlank(),
                new Length(['min' => 10, 'max' => 768]),
            ],
            ])
            ->add('save', SubmitType::class, ['label' => 'Wyślij']);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Contact::class,
        ]);
    }
}