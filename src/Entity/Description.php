<?php

namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="description")
 * @UniqueEntity(fields={"description"})
 */
class Description
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer", nullable=false)
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="`description`", length=768, type="string", nullable=false, unique=true)
     * @Assert\NotBlank()
     * @Assert\Length(min="10", max="2048")
     */
    private $description;

    /**
     * @var string|null
     * @ORM\Column(name="`description_eng`", length=768, type="string", nullable=true)
     * @Assert\Length(min="10", max="2048")
     */
    private $descriptionENG;

    /**
     * @var Content|null
     * @ORM\ManyToOne(targetEntity="Content", inversedBy="descriptions")
     */
    private $content;

    /**
     * @var \DateTime
     * @ORM\Column(name="`created_at`", type="datetime", nullable=false)
     * @Assert\Type(type="\DateTime")
     */
    private $createdAt;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    public function getDescriptionENG(): ?string
    {
        return $this->descriptionENG;
    }

    public function setDescriptionENG(?string $descriptionENG): void
    {
        $this->descriptionENG = $descriptionENG;
    }


    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    /**
     * Get the value of content
     *
     * @return  Content|null
     */
    public function getContent(): ?Content
    {
        return $this->content;
    }

    /**
     * @param Content|null $content
     */
    public function setContent(Content $content): void
    {
        $this->content = $content;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    public function __toString()
    {
        return $this->description;
    }
}
