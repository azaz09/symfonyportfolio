<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="page")
 * @UniqueEntity(fields={"title", "titleEng"})
 */
class Page
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer", nullable=false)
     * @var int
     */
    private $id;

    /**
     * @Assert\Length(min="3")
     * @ORM\Column(name="title", type="string", length=64, unique=true)
     * @var string
     */
    private $title;

    /**
     * @Gedmo\Slug(fields={"title"})
     * @ORM\Column(type="string", length=128, unique=true)
     */
    private $slug;

    /**
     * @Assert\Length(min="3")
     * @ORM\Column(name="titleEng", type="string", length=64, unique=true)
     * @var string
     */
    private $titleEng;

    /**
     * @ORM\OneToMany(targetEntity="Content", mappedBy="page")
     * @var Content[]Collection
     */
    private $contents;

    /**
     * @var bool|null
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $language;

    /**
     * @var bool|null
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $enabled;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=false)
     * @Assert\Type(type="\DateTime")
     */
    private $createdAt;

    public function __construct()
    {
        $this->contents = new ArrayCollection();
        $this->createdAt = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): void
    {
        $this->title = $title;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): void
    {
        $this->slug = $slug;
    }

    public function getTitleEng(): ?string
    {
        return $this->titleEng;
    }

    public function setTitleEng(?string $titleEng): void
    {
        $this->titleEng = $titleEng;
    }

    public function getInfo()
    {
        return $this->contents;
    }

    /**
     * @return Content[]Collection
     */
    public function getContents()
    {
        return $this->contents;
    }

    public function addContent(Content $content): void
    {
        $content->setPage($this);
        $this->contents->add($content);
    }

    public function getEnabled(): ?bool
    {
        return $this->enabled;
    }

    public function setEnabled(?bool $enabled): void
    {
        $this->enabled = $enabled;
    }

    public function getLanguage(): ?bool
    {
        return $this->language;
    }

    public function setLanguage(?bool $language): void
    {
        $this->language = $language;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    public function __toString()
    {
        return $this->title;
    }

    public function removeContent(Content $content): self
    {
        if ($this->contents->contains($content)) {
            $this->contents->removeElement($content);
            // set the owning side to null (unless already changed)
            if ($content->getPage() === $this) {
                $content->setPage(null);
            }
        }

        return $this;
    }
}