<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="content")
 * @UniqueEntity(fields={"header", "headerENG"})
 */
class Content
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer", nullable=false)
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false, length=64, unique=true)
     * @Assert\NotBlank()
     * @Assert\Length(min="3", max="64")
     */
    private $header;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false, length=64, unique=true)
     * @Assert\NotBlank()
     * @Assert\Length(min="3", max="64")
     */
    private $headerENG;

    /**
     * @var Image[]Collection
     * @ORM\OneToMany(targetEntity="Image", mappedBy="content")
     */
    private $images;

    /**
     * @var Description[]Collection
     * @ORM\OneToMany(targetEntity="Description", mappedBy="content")
     */
    private $descriptions;


    /**
     * @ORM\ManyToOne(targetEntity="Page", inversedBy="contents")
     * @var Page|null
     */
    private $page;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=false)
     * @Assert\Type(type="\DateTime")
     */
    private $createdAt;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->images = new ArrayCollection();
        $this->descriptions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return Description[]Collection
     */
    public function getDescriptions()
    {
        return $this->descriptions;
    }

    public function addDescription(Description $description): void
    {
        $description->setContent($description);
        $this->descriptions->add($description);

    }

    /**
     * @return Image[]Collection
     */
    public function getImages()
    {
        return $this->images;
    }

    public function addImage(Image $image): void
    {
        $image->setContent($this);
        $this->images->add($image);
    }

    public function getHeaderENG(): ?string
    {
        return $this->headerENG;
    }

    public function setHeaderENG(?string $headerENG): void
    {
        $this->headerENG = $headerENG;
    }

    public function getHeader(): ?string
    {
        return $this->header;
    }

    public function setHeader(?string $header): void
    {
        $this->header = $header;
    }

    public function getPage(): ?Page
    {
        return $this->page;
    }

    /**
     * @param Page $page
     */
    public function setPage(Page $page): void
    {
        $this->page = $page;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    public function __toString()
    {
        return $this->header;
    }

    # /**
    #  * @var int|null
    #  * @ORM\OneToOne(targetEntity="App\Entity\ContentType", inversedBy="ContentType", cascade={"persist"})
    #  * @ORM\JoinColumn(name="contentType_id", referencedColumnName="id")
    #  */
    # private $contentType;

    #public function setContentType(ContentType $contentType)
    #{
    #    $this->contentType = $contentType;
    #    $this->contentType->setContent($this);
    #}

    #public function getContentType()
    #{
    #    return $this->contentType;
    #}


    public function removeImage(Image $image): self
    {
        if ($this->images->contains($image)) {
            $this->images->removeElement($image);
            // set the owning side to null (unless already changed)
            if ($image->getContent() === $this) {
                $image->setContent(null);
            }
        }

        return $this;
    }

    public function removeDescription(Description $description): self
    {
        if ($this->descriptions->contains($description)) {
            $this->descriptions->removeElement($description);
            // set the owning side to null (unless already changed)
            if ($description->getContent() === $this) {
                $description->setContent(null);
            }
        }

        return $this;
    }
}
