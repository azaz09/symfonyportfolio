<?php


namespace App\Entity;
use Symfony\Component\Validator\Constraints as Assert;

class Contact
{
    /**
     * @Assert\NotBlank
     * @Assert\Email(message="Wprowadź poprawny adres email")
     * @Assert\Length(min="6", max="150")
     */
    public $email;

    /**
     * @Assert\NotBlank
     * @Assert\Length(min="5", max="40", minMessage="To pole musi zawierać min. 5 znaków", maxMessage="Temat nie może przekraczać 40 znaków")
     */
    public $question;

    /**
     * @Assert\NotBlank
     * @Assert\Length(min="10", max="768", minMessage="Wiadomość musi mieć min. 10 znaków", maxMessage="Za długie! Zmieść się w 20 słowach.")
     */
    public $theme;

}