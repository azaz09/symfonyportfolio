<?php

namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @ORM\Entity
 * @Vich\Uploadable
 * @ORM\Table(name="image")
 * @UniqueEntity(fields={"title", "imageName"})
 */
class Image
{
    /**
     * @var integer
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true, unique=true)
     * @Assert\Length(min="3")
     */
    private $title;

    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true, unique=true)
     * @Assert\Length(min="3")
     */
    private $imageName;

    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     * @Assert\Length(min="4")
     */
    private $source;

    /**
     * @Vich\UploadableField(mapping="page_image", fileNameProperty="imageName", size="imageSize")
     * @Assert\File(
     *      maxSize="5242880",
     *      mimeTypes = {
     *          "image/webp",
     *          "image/png",
     *          "image/jpeg",
     *          "image/jpg",
     *          "image/gif",
     *          "application/pdf",
     *          "application/x-pdf"
     *      }
     * )
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="integer", nullable=true)
     *
     * @var integer|null
     */
    private $imageSize;

    /**
     * @var bool|null
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $type;

    /**
     * @var string
     * @Assert\NotBlank
     * @ORM\Column(type="string", nullable=false)
     * @Assert\Length(min="1")
     */
    private $link;

    /**
     * @var Content|null
     * @ORM\ManyToOne(targetEntity="Content", inversedBy="images")
     */
    private $content;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=false)
     * @Assert\Type(type="\DateTime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=false)
     * @Assert\Type(type="\DateTime")
     */
    private $updatedAt;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime('now');
    }

    /**
     * Get the value of id
     *
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @param int $id
     *
     * @return  self
     */
    public function setId(?int $id)
    {
        return $this->id = $id;
    }

    /**
     * Get the value of source
     *
     * @return  string
     */
    public function getSource(): ?string
    {
        return $this->source;
    }

    /**
     * Set the value of source
     *
     * @param string $source
     *
     * @return  self
     */
    public function setSource(string $source)
    {
        return $this->source = $source;
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    /**
     *
     */
    public function setImageFile(?File $image): void
    {
        $this->imageFile = $image;
        if ($image) {

            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getImageSize(): ?int
    {
        return $this->imageSize;
    }

    public function setImageSize(?int $imageSize): void
    {
        $this->imageSize = $imageSize;
    }

    /**
     * Get the value of type
     *
     * @return  bool|null
     */
    public function getType(): ?bool
    {
        return $this->type;
    }

    /**
     * Set the value of type
     *
     * @param bool|null $type
     *
     * @return  self
     */
    public function setType(?bool $type): void
    {
        $this->type = $type;
    }

    /**
     * Get the value of link
     *
     * @return  string
     */
    public function getLink(): ?string
    {
        return $this->link;
    }

    /**
     * Set the value of link
     *
     * @param string $link
     *
     * @return  self
     */
    public function setLink(string $link): void
    {
        $this->link = $link;
    }

    /**
     * Get the value of title
     *
     * @return  string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * Set the value of title
     *
     * @param string $title
     *
     * @return  self
     */
    public function setTitle(string $title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getImageName(): ?string
    {
        return $this->imageName;
    }

    /**
     * @param string|null $imageName
     * @return Image
     */
    public function setImageName(?string $imageName): Image
    {
        $this->imageName = $imageName;
        return $this;
    }


    /**
     * Get the value of contentType
     *
     * @return  Content|null
     */
    public function getContent(): ?Content
    {
        return $this->content;
    }

    /**
     * @param Content|null $content
     */
    public function setContent(Content $content): void
    {
        $this->content = $content;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTime $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    public function __toString()
    {
        return $this->title;
    }
}
