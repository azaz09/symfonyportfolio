-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: localhost    Database: ajportfolio
-- ------------------------------------------------------
-- Server version	8.0.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `content`
--

DROP TABLE IF EXISTS `content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `content` (
  `id` int NOT NULL AUTO_INCREMENT,
  `page_id` int DEFAULT NULL,
  `header` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `header_eng` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_FEC530A96E72A8C1` (`header`),
  UNIQUE KEY `UNIQ_FEC530A97A07A961` (`header_eng`),
  KEY `IDX_FEC530A9C4663E4` (`page_id`),
  CONSTRAINT `FK_FEC530A9C4663E4` FOREIGN KEY (`page_id`) REFERENCES `page` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content`
--

LOCK TABLES `content` WRITE;
/*!40000 ALTER TABLE `content` DISABLE KEYS */;
INSERT INTO `content` VALUES (2,1,'Projekty','Projects','2020-05-19 15:49:00'),(4,4,'Technologie / Biblioteki / Frameworki','Tech','2020-05-21 22:49:00'),(5,4,'obserwowane','observed','2020-05-23 15:39:00'),(6,3,'Kim jestem','Life','2020-05-24 12:00:00'),(7,5,'kontakt','contact','2020-05-26 19:24:00'),(8,5,'motto','motto','2020-05-26 20:34:00'),(9,5,'Polecane linki','Follows','2020-05-26 20:37:00');
/*!40000 ALTER TABLE `content` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `description`
--

DROP TABLE IF EXISTS `description`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `description` (
  `id` int NOT NULL AUTO_INCREMENT,
  `content_id` int DEFAULT NULL,
  `description` varchar(768) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_eng` varchar(768) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_6DE440263782B5DB` (`description`),
  KEY `IDX_6DE4402684A0A3ED` (`content_id`),
  CONSTRAINT `FK_6DE4402684A0A3ED` FOREIGN KEY (`content_id`) REFERENCES `content` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `description`
--

LOCK TABLES `description` WRITE;
/*!40000 ALTER TABLE `description` DISABLE KEYS */;
INSERT INTO `description` VALUES (1,4,'<p>Wykorzystywane</p>','<p>[...]</p>','2020-05-22 20:47:00'),(2,4,'<p>Obserwowane</p>',NULL,'2020-05-23 14:34:00'),(3,4,'<p>Pozostałe</p>',NULL,'2020-05-23 16:49:00'),(4,6,'<p style=\"text-align:justify\">Albert Jurasik - Technik Informatyk z zawodu. Pasjonuje się programowaniem i informatyką od małego.</p>\r\n\r\n<p style=\"text-align:justify\">Pierwszym językiem, w którym uczyłem się pisać był c++.&nbsp;Od 2015&nbsp;programuje i rozwijam się w PHP. Przez pewien czas pisałem we&nbsp;frameworku Yii2. Obecnie od roku siedzę przy symfony.</p>\r\n\r\n<p style=\"text-align:justify\">Staram się dbać o dobrą rozszerzalność i uczyć się z każdym nowym kodem.</p>\r\n\r\n<p style=\"text-align:justify\">Interesują mnie projekty, z których mogę wynieść coś nowego - tworząc przy tym przydatną aplikacje.</p>\r\n\r\n<p style=\"text-align:justify\">Moim celem jest rozwój i satysfakcja z posiadanych pasji.</p>',NULL,'2020-05-24 13:16:00'),(6,7,'<address>\r\n<div>\r\n<p>e-mail: albertjurasik@gmail.com</p>\r\n\r\n<p>Nr. tel: +48<a class=\"isDisabled\" href=\"+48 577 139 369\"> 577 139 369</a></p>\r\n\r\n</div>\r\n</address>',NULL,'2020-05-26 20:39:00'),(7,8,'<p>Nie porównuj siebie do innych - jedyną osobą, od której powinieneś być lepszy jesteś ty sam z dnia wczorajszego.</p>',NULL,'2020-05-26 20:41:00'),(8,9,'<p><a href=\"https://www.youtube.com/user/MiroslawZelent\" target=\"_blank\">Pasja Informatyki</a></p>\r\n\r\n<p><a href=\"https://www.youtube.com/channel/UCq8XmOMtrUCb8FcFHQEd8_g\">Hello Roman</a></p>\r\n\r\n<p><a href=\"https://www.youtube.com/channel/UCSJbGtTlrDami-tDGPUV9-w\">Academind</a></p>\r\n\r\n<p><a href=\"https://www.youtube.com/channel/UCD3KVjbb7aq2OiOffuungzw\">DarkCode</a></p>\r\n\r\n<p><a href=\"http://forum.php.pl/\">Forum PHP</a></p>',NULL,'2020-05-26 20:43:00');
/*!40000 ALTER TABLE `description` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `image`
--

DROP TABLE IF EXISTS `image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `image` (
  `id` int NOT NULL AUTO_INCREMENT,
  `content_id` int DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `source` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_size` int DEFAULT NULL,
  `type` tinyint(1) DEFAULT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_C53D045F2B36786B` (`title`),
  UNIQUE KEY `UNIQ_C53D045FAC199498` (`image_name`),
  KEY `IDX_C53D045F84A0A3ED` (`content_id`),
  CONSTRAINT `FK_C53D045F84A0A3ED` FOREIGN KEY (`content_id`) REFERENCES `content` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `image`
--

LOCK TABLES `image` WRITE;
/*!40000 ALTER TABLE `image` DISABLE KEYS */;
INSERT INTO `image` VALUES (2,2,'PodstawyJS','podstawyjs.webp','js.webp',67108,1,'http://podstawyjs.cba.pl/','2020-05-19 15:50:00','2020-05-30 20:35:31'),(3,NULL,'Banner','theme3.webp',NULL,506318,0,'#','2020-05-19 15:53:00','2020-05-30 20:35:09'),(5,2,'TI_Start','tistart.webp','data.webp',156270,1,'http://tistart.cba.pl','2020-05-20 13:12:00','2020-05-30 20:33:01'),(6,2,'Projekty w Repozytorium','PozostaleProjekty.webp','sketch.webp',47982,1,'https://bitbucket.org/azaz09/','2020-05-21 14:55:00','2020-05-30 20:32:39'),(7,2,'Profil Symfony','symfonypr.webp','symfony.webp',98750,1,'https://fontawesome.com/icons/symfony','2020-05-21 20:17:00','2020-05-30 20:24:40');
/*!40000 ALTER TABLE `image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration_versions`
--

DROP TABLE IF EXISTS `migration_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `migration_versions` (
  `version` varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `executed_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration_versions`
--

LOCK TABLES `migration_versions` WRITE;
/*!40000 ALTER TABLE `migration_versions` DISABLE KEYS */;
/*!40000 ALTER TABLE `migration_versions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page`
--

DROP TABLE IF EXISTS `page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `page` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `titleEng` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `language` tinyint(1) DEFAULT NULL,
  `enabled` tinyint(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `slug` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_140AB6202B36786B` (`title`),
  UNIQUE KEY `UNIQ_140AB620CD666666` (`titleEng`),
  UNIQUE KEY `UNIQ_140AB620989D9B62` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page`
--

LOCK TABLES `page` WRITE;
/*!40000 ALTER TABLE `page` DISABLE KEYS */;
INSERT INTO `page` VALUES (1,'projekty','projects',1,1,'2020-05-18 19:57:00','projekty'),(2,'zdjęcia','pictures',1,0,'2020-05-18 19:58:00','zdjecia'),(3,'o mnie','AboutMe',1,1,'2020-05-18 19:59:00','o-mnie'),(4,'tech','tech',1,1,'2020-05-18 20:00:00','technologie'),(5,'kontakt','contact',1,1,'2020-05-18 20:01:00','kontakt');
/*!40000 ALTER TABLE `page` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8D93D649F85E0677` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (10,'admin','a:0:{}','$argon2id$v=19$m=65536,t=4,p=1$dlV3ZGVMMVBWN3Q2R0pJRg$8ZV79Qc+ZfXyQLKQ51e+719820if0u5mV2gBLMGUD8Q'),(11,'admin123','a:0:{}','$argon2id$v=19$m=65536,t=4,p=1$L1Qzb3BTckM3Ti9mZHY2TA$HEuUVcVB7rhvQ4vd5xJgJb3awmUGzzwU2tjEggJFE34');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-05-30 22:54:41
